import logging

from django.http import Http404, JsonResponse
from django.core.files.storage import default_storage as storage

import django_rq
from rq.job import Job

logger = logging.getLogger(__name__)

def job_status(request):
  try:
    conn = django_rq.get_connection('default')
    results = []
    for job_id in request.GET.getlist('job_ids'):
      try:
        job = Job.fetch(job_id, connection=conn)

        result = {
          'job_id': job_id,
          'status': job.get_status()
        }

        if(job.get_status() == 'finished'):
          result['url'] = storage.url(job.result)

        results.append(result)
      except Exception as e:
        logger.warn('Unable to retrieve job {}'.format(job_id), e)

    return JsonResponse({ 'jobs': results })
  except Exception as e:
    logger.warn('Error retrieving jobs {}', e)
    raise Http404