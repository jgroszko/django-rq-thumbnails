import logging

from django.conf import settings
from django.template import Library
from django.core.files.storage import default_storage as storage
from django.conf import settings
from django.core.cache import cache

from ..utils import make_thumbnail, get_paths
from ..settings import DefaultSettings

logger = logging.getLogger(__name__)
register = Library()

@register.simple_tag
def thumbnail(image, size):
  cache_key, thumb_path = get_paths(image, size)

  result = cache.get(cache_key)
  if result is None:
    logger.debug('Cache miss {}'.format(cache_key))

    job = make_thumbnail.delay(image, size)
    return '{}{}?job={}'.format(
      settings.STATIC_URL,
      getattr(settings, 'RQTHUMBNAILS_PLACEHOLDER', DefaultSettings.RQTHUMBNAILS_PLACEHOLDER),
      job.id
    )

  return storage.url(thumb_path)
