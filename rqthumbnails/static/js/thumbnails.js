window.onload = () => {
  if(!fetch) return;

  const INTERVAL = 1000;

  var images = document.getElementsByTagName('img');
  const re = /^\S+\?job=(\S+)$/;

  const check_jobs = (jobs) => {
    let url = '/thumbnails/?';
    let i = 0;
    while(i < jobs.length && url.length < 1900) {
      url += 'job_ids=' + jobs[i].job_id + '&';
      i++;
    }

    fetch(url).then(response => {
      return response.json();
    }).then(({ jobs: response_jobs }) => {
      const finished = response_jobs.filter(job => job.status === 'finished');

      finished.forEach(response_job => {
        const job = jobs.find(j => j.job_id === response_job.job_id);

        job.elem.src = response_job.url;
      });

      const complete_job_ids = finished.map(j => j.job_id);
      const incomplete = jobs.filter(j => !complete_job_ids.includes(j.job_id));
      if(incomplete.length)
        window.setTimeout(() => check_jobs(incomplete), INTERVAL);
    });
  };

  const jobs = Array.from(images).reduce((accumulator, imageElem) => {
    var match = imageElem.src.match(re);
    if(match) {
      return [...accumulator, {
        'elem': imageElem,
        'job_id': match[1]
      }];
    }

    return accumulator;
  }, []);

  if(jobs.length)
    window.setTimeout(() => check_jobs(jobs), INTERVAL);
};
