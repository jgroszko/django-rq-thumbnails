import os
from io import BytesIO
from PIL import Image
import logging

from django.conf import settings
from django.core.files.storage import default_storage as storage
from django.core.cache import cache

from django_rq import job

from .settings import DefaultSettings

logger = logging.getLogger(__name__)

def get_paths(image, size):
  name, ext = os.path.splitext(os.path.basename(image.name))
  dir = os.path.dirname(image.name)
  thumb_name = '{}-{}{}'.format(name, size, ext)
  thumbs_dir = getattr(settings, 'RQTHUMBNAILS_DIR', DefaultSettings.RQTHUMBNAILS_DIR)
  thumb_path = os.path.join(dir, thumbs_dir, thumb_name)
  cache_prefix = getattr(settings, 'RQTHUMBNAILS_CACHE_PREFIX', DefaultSettings.RQTHUMBNAILS_CACHE_PREFIX)
  cache_key = '{}:{}:{}'.format(cache_prefix, image.name, size)

  return (cache_key, thumb_path)

@job
def make_thumbnail(image, size):
  cache_key, thumb_path = get_paths(image, size)

  if storage.exists(thumb_path):
      logger.debug('Found in storage {}'.format(thumb_path))
  else:
    original_file = storage.open(image.name, 'r')
    im = Image.open(original_file)
    im.thumbnail([int(x) for x in size.split('x')])

    thumb = storage.open(thumb_path, 'w')
    buffer = BytesIO()
    im.save(buffer, format=im.format)
    thumb.write(buffer.getvalue())

    original_file.close()
    thumb.close()

  cache.set(cache_key, thumb_path, timeout=None)
  return thumb_path
