class DefaultSettings:
  RQTHUMBNAILS_PLACEHOLDER = 'img/placeholder.png'

  RQTHUMBNAILS_QUEUE = 'default'

  RQTHUMBNAILS_DIR = '.thumbnails'

  RQTHUMBNAILS_CACHE_PREFIX = 'thumbnails'
