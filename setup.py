from setuptools import setup, find_packages

setup(
	name="django-rq-thumbnails",
	version="0.1",
	author="John Groszko",
	author_email="john@tinythunk.com",
	license="MIT",
	packages=find_packages(),
	install_requires=['Pillow', 'django-rq'],
)
